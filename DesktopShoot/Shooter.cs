﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace DesktopShoot
{
    class Shooter
    {
        PictureBox picture;

        public Shooter(Form form, string path)
        {
            picture = new PictureBox
            {
                ImageLocation = path,
                Location = new Point(0,0)
            };
            form.Controls.Add(picture);

            UpdateTimer.Instance.AddUpdateProcess(update());

        }
        // TODO:ここはSTA呼び出ししないと落ちちゃいます！

        IEnumerator<bool> update()
        {
            while (true)
            {
                Point p = picture.Location;
                if (Keyboard.IsKeyDown(Key.W))
                    p.Y -= 4;
                if (Keyboard.IsKeyDown(Key.A))
                    p.X -= 4;
                if (Keyboard.IsKeyDown(Key.S))
                    p.Y += 4;
                if (Keyboard.IsKeyDown(Key.D))
                    p.X += 4;
                picture.Location = p;

                yield return true;
            }


        }

        
    }
}
