﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace DesktopShoot
{
    class UpdateTimer
    {
        static UpdateTimer instance;
        public static UpdateTimer Instance
        {
            get
            {
                if (instance == null)
                    instance = new UpdateTimer();
                return instance;
            }
        }

        Timer timer;
        int framecount;
        public int Framecount { get { return framecount; } }
        List<IEnumerator<bool>> coroutines;

        UpdateTimer()
        {
            Console.WriteLine("Timer Started.");
            timer = new Timer(1000 / 60.0);
            timer.Elapsed += timer_Elapsed;
            timer.Start();
            framecount = 0;
            coroutines = new List<IEnumerator<bool>>();
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            ++framecount;
            Console.WriteLine(coroutines.Count);
            Update();
        }

        void Update()
        {
            List<IEnumerator<bool>> finishedprocess = new List<IEnumerator<bool>>();

            foreach (var c in coroutines)
            {
                if (!c.MoveNext())
                    finishedprocess.Add(c);
            }

            foreach (var p in finishedprocess)
            {
                coroutines.Remove(p);
            }
        }

        public void AddUpdateProcess(IEnumerator<bool> p)
        {
            coroutines.Add(p);
        }
    }
}
