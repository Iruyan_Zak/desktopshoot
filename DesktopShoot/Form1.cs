﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DesktopShoot
{
    public partial class Form1 : Form
    {
        Shooter shooter;

        public Form1()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.None;
            configureEvent();

            this.TransparencyKey = this.BackColor = Color.FromArgb(255, 254, 255, 255);
        }

        void configureEvent()
        {
            this.Load += Form1_Load;
            this.KeyDown += Form1_KeyDown;
        }

        void Form1_Load(object sender, EventArgs e)
        {
            shooter = new Shooter(this, @"D:\Users\Yuri\Desktop\Pictures\Plane.png");
        }

        void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }
    }
}
